package aristaeus

import (
	"strings"
)

// Folder Type for nested folder hierarchy
type Folder struct {
	Name     string    `json:"name"`
	Children []*Folder `json:"children"`
}

// uploadSource prints the tree to stdout
func createFolder(itt *HashIterator, path string) (*Folder, error) {
	var n Node
	var err error

	if n, err = itt.NodeAt(path); err != nil {
		return nil, err
	}

	parts := strings.Split(path, "/")
	name := parts[len(parts)-1]

	f := &Folder{Name: name}
	for _, path := range n.Children() {
		var child *Folder

		if child, err = createFolder(itt, path); err != nil {
			return nil, err
		}

		f.Children = append(f.Children, child)
	}
	return f, nil
}
