package aristaeus

import (
	"time"

	"github.com/google/uuid"
)

// const values for Registry Types
const (
	RegNone                     = 0 // No defined value type.
	RegSz                       = 1 // Null-terminated string.
	RegExpandSz                 = 2 // Null-terminated string that contains unexpanded references to environment variables (for example, "%PATH%").
	RegBinary                   = 3 // Binary data in any form.,
	RegDword                    = 4 // 32-bit number.
	RegDWordBigEndian           = 5 // 32-bit number in big-endian format.
	RegLink                     = 6 // Unicode symbolic link.
	RegMultiSz                  = 7 // Array of null-terminated strings that are terminated by two null characters.
	RegResourceList             = 8 // Device-driver resource list.
	RegFullResourceDescriptor   = 9
	RegResourceRequirementsList = 10
	RegQword                    = 11 // 64-bit number.
)

// Hive stores information from parsed registry hive
type Hive struct {
	ID             string                `json:"id"`
	Map            map[string]RegNodePtr `json:"map,omitempty"`
	Type           string                `json:"type,omitempty"`
	Label          string                `json:"label"`
	Description    string                `json:"description"`
	Classification string                `json:"classification"`
	Hostname       string                `json:"hostname"`
	OpNumber       string                `json:"op_number"`
	DateAcquired   time.Time             `json:"date_acquired"`
	Created        time.Time             `json:"created"`
	CreatedBy      string                `json:"created_by"`
}

// CreateHive creates a new instance of Hive structure
func CreateHive(label, description, classification, hostname, opNumber, createdBy string, dateAcquired time.Time) *Hive {
	uuid := uuid.New()
	hive := &Hive{
		Type:           "hive",
		ID:             uuid.String(),
		Label:          label,
		Description:    description,
		Classification: classification,
		DateAcquired:   dateAcquired,
		Hostname:       hostname,
		OpNumber:       opNumber,
		CreatedBy:      createdBy,
		Created:        time.Now(),
	}
	hive.Map = make(map[string]RegNodePtr)
	return hive
}

// HiveDiff stores information from parsed registry hive
type HiveDiff struct {
	ID          string   `json:"_id"`
	Changes     []Change `json:"changes"`
	Type        string   `json:"type"`
	Label       string   `json:"label"`
	Description string   `json:"description"`
}

// CreateHiveDiff creates a new instance of a Hive Diff structure
func CreateHiveDiff(label, description string, changes []Change) *HiveDiff {
	uuid := uuid.New()
	hd := &HiveDiff{Type: "delta", Label: label, Description: description, ID: uuid.String(), Changes: changes}
	return hd
}
