package aristaeus

import (
	"encoding/binary"
	"errors"
	"io"
)

type memoryReader struct {
	offset uint32
	reader io.ReadSeeker
}

func (mr *memoryReader) raw() ([]byte, error) {
	var size int32
	var err error
	if size, err = mr.size(); err != nil {
		return nil, err
	}
	return mr.readBytes(0, int(size))
}

func (mr *memoryReader) readBytes(skip uint32, length int) ([]byte, error) {
	var off int64
	var br int
	var err error

	ptr := int64(skip + mr.offset)
	if off, err = mr.reader.Seek(ptr, 0); err != nil {
		return nil, err
	}
	if off != ptr {
		return nil, errors.New("Failed to seek to correct offset")
	}
	result := make([]byte, length)
	if br, err = mr.reader.Read(result); err != nil {
		return nil, err
	}

	if length != br {
		return nil, errors.New("Failed to read complete byte array")
	}

	return result, nil
}

func (mr *memoryReader) readBytesFromStart(offset int64, length int) ([]byte, error) {
	var off int64
	var br int
	var err error

	if off, err = mr.reader.Seek(offset, 0); err != nil {
		return nil, err
	}
	if off != offset {
		return nil, errors.New("Failed to seek to correct offset")
	}
	result := make([]byte, length)
	if br, err = mr.reader.Read(result); err != nil {
		return nil, err
	}

	if length != br {
		return nil, errors.New("Failed to read complete byte array")
	}

	return result, nil
}

func (mr *memoryReader) readUint64(skip uint32) (uint64, error) {
	var off int64
	var result uint64
	var err error

	ptr := int64(skip + mr.offset)
	if off, err = mr.reader.Seek(ptr, 0); err != nil {
		return 0, err
	}
	if off != ptr {
		return 0, errors.New("Failed to seek to correct offset")
	}

	if err = binary.Read(mr.reader, binary.LittleEndian, &result); err != nil {
		return 0, err
	}

	return result, nil
}

func (mr *memoryReader) readUint32(skip uint32) (uint32, error) {
	var off int64
	var result uint32
	var err error

	ptr := int64(skip + mr.offset)
	if off, err = mr.reader.Seek(ptr, 0); err != nil {
		return 0, err
	}
	if off != ptr {
		return 0, errors.New("Failed to seek to correct offset")
	}

	if err = binary.Read(mr.reader, binary.LittleEndian, &result); err != nil {
		return 0, err
	}

	return result, nil
}

func (mr *memoryReader) readUint16(skip uint32) (uint16, error) {
	var off int64
	var result uint16
	var err error

	ptr := int64(skip + mr.offset)
	if off, err = mr.reader.Seek(ptr, 0); err != nil {
		return 0, err
	}
	if off != ptr {
		return 0, errors.New("Failed to seek to correct offset")
	}

	if err = binary.Read(mr.reader, binary.LittleEndian, &result); err != nil {
		return 0, err
	}

	return result, nil
}

func (mr *memoryReader) size() (int32, error) {
	var result int32
	var err error
	var sz uint32
	if sz, err = mr.readUint32(0); err != nil {
		return 0, err
	}

	result = int32(sz)

	if result == 0 {
		return 0, errors.New("Invalid record")
	}
	if result < 0 {
		result = -result
	}
	return result, nil
}

// Used returns whether this registry key is in use
func (mr *memoryReader) Used() (bool, error) {
	var sz uint32
	var err error
	if sz, err = mr.readUint32(0); err != nil {
		return false, err
	}

	if sz == 0 {
		return false, errors.New("Invalid record")
	}
	return int32(sz) < 0, nil
}

func (mr *memoryReader) sig() (result uint16, err error) {
	if result, err = mr.readUint16(0x4); err != nil {
		return 0, err
	}
	return result, nil
}
