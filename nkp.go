package aristaeus

import (
	"crypto/sha512"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"time"
)

// Nkp lightweight point to a registry key, which lazy reads from the underlying data when queried
type Nkp struct {
	memoryReader
	basePath string
}

// SubKeyPtr lightweight point to a sub key, which lazy reads from the underlying data
type subKeyPtr struct {
	memoryReader
}

func (skp *subKeyPtr) numEntries() (uint16, error) {
	return skp.readUint16(0x6)
}

func (skp *subKeyPtr) nkOffsets() ([]uint32, error) {
	var num, sig uint16
	var err error
	if num, err = skp.numEntries(); err != nil {
		return nil, err
	}

	result := make([]uint32, num)
	var ptr uint32 = 0x8

	if sig, err = skp.sig(); err != nil {
		return nil, err
	}

	if sig == 0x666C || sig == 0x686C { // lf or lh
		//if sig == 0x696C || sig == 0x686C { // li or lh
		for i := range result {
			if result[i], err = skp.readUint32(ptr); err != nil {
				return nil, err
			}

			ptr += 8 // skip offset and str or hash
		}
	} else if sig == 0x6972 { // ri
		result = []uint32{}

		for i := 0; i < int(num); i++ {
			var skoff uint32
			var coff []uint32
			if skoff, err = skp.readUint32(ptr); err != nil {
				return nil, err
			}

			sk := createSubkeyPtr(skp.reader, skoff)

			if coff, err = sk.nkOffsets(); err != nil {
				return nil, err
			}

			result = append(result, coff...)

			ptr += 4 // skip offset
		}

	} else if sig == 0x696C { // li

		for i := range result {
			if result[i], err = skp.readUint32(ptr); err != nil {
				return nil, err
			}

			ptr += 4 // skip offset
		}
		//} else if sig == 0x666C { // lf

	} else {
		fmt.Printf("SIG: 0x%04x at offset 0x%08x\n", sig, skp.offset)
		b, _ := skp.readBytes(0, 16)

		hd(b)
		return nil, errors.New("unrecognised format")
	}

	return result, nil
}

func createSubkeyPtr(reader io.ReadSeeker, offset uint32) *subKeyPtr {
	skp := &subKeyPtr{}
	skp.reader = reader
	skp.offset = offset

	return skp
}

// Children returns the subkeys for this reg key
func (nkp *Nkp) Children() ([]*Nkp, error) {
	var numSubKeys, subkeyOffset uint32
	var result []*Nkp
	var path string
	var err error

	if numSubKeys, err = nkp.numSubKeys(); err != nil {
		return nil, err
	}
	result = make([]*Nkp, numSubKeys)

	if subkeyOffset, err = nkp.offsetToSubkeyList(); err != nil {
		return nil, err
	}

	if path, err = nkp.Path(); err != nil {
		return nil, err
	}

	if numSubKeys > 0 {
		nkp.reader.Seek(int64(subkeyOffset), 0)
		sk := createSubkeyPtr(nkp.reader, subkeyOffset)

		if offsets, err := sk.nkOffsets(); err == nil {
			for i, v := range offsets {
				result[i] = createNkp(nkp.reader, v, path)
			}
		} else {
			if raw, err := nkp.raw(); err == nil {
				hd(raw)
			}
			fmt.Println("FAILED TO PARSE SUBKEYS", err)
		}
	}

	return result, nil
}

// Values returns the values contained in this reg key
func (nkp *Nkp) Values() ([]*Vkp, error) {
	var numValues, valuesOffset uint32
	var result []*Vkp
	var offsets []uint32
	var err error
	var path string

	if numValues, err = nkp.numValues(); err != nil {
		return nil, err
	}

	if numValues == 0 {
		return []*Vkp{}, nil
	}

	result = make([]*Vkp, numValues)

	if valuesOffset, err = nkp.offsetToValueList(); err != nil {
		return nil, err
	}

	vlp := &valueListPtr{numEntries: numValues}
	vlp.offset = valuesOffset
	vlp.reader = nkp.reader

	if offsets, err = vlp.valueOffsets(); err != nil {
		return nil, err
	}

	if path, err = nkp.Path(); err != nil {
		return nil, err
	}

	for i, k := range offsets {
		if s, e := nkp.reader.Seek(int64(k), 0); e != nil && s != int64(k) {
			return nil, errors.New("Failed to seek to value offset")
		}

		result[i] = createVkp(nkp.reader, k, path)
	}

	return result, nil
}

// Hash calculates the hash of the node
func (nkp *Nkp) Hash(hashmap map[string]RegNodePtr) (string, error) {
	var name, path, className string
	var err error
	var timestamp time.Time

	if path, err = nkp.Path(); err != nil {
		return "", err
	}

	if name, err = nkp.Name(); err != nil {
		return "", err
	}

	if className, err = nkp.ClassName(); err != nil {
		return "", err
	}

	h := sha512.New()

	// key hashes it's own path?
	h.Write([]byte(path))
	h.Write([]byte(className)) //TODO: this needs to factor into the compare?

	var childPaths []string
	var values []*Vkp
	var children []*Nkp

	if values, err = nkp.Values(); err != nil {
		return "", err
	}

	for _, value := range values {
		var valueName, hash, dataHash, valuePath string
		var ds, dt uint16
		var data []byte
		//if name, err = value.Name(); err != nil {
		//	return "", nil
		//}

		if hash, err = value.Hash(); err != nil {
			return "", err
		}
		h.Write([]byte(hash))

		if dt, err = value.DataType(); err != nil {
			return "", err
		}

		if ds, err = value.dataLength(); err != nil {
			return "", err
		}

		if dataHash, err = value.DataHash(); err != nil {
			return "", err
		}

		if data, err = value.Data(); err != nil {
			return "", err
		}

		if valuePath, err = value.Path(); err != nil {
			return "", err
		}

		if valueName, err = value.Name(); err != nil {
			return "", err
		}

		//valuePath := fmt.Sprintf("%s/%s", path, name)

		v := CreateRegNodeValue(valueName, valuePath, hash, dt, ds, dataHash, data)
		hashmap[valuePath] = v
		childPaths = append(childPaths, valuePath)
	}

	//var childPaths []string
	if children, err = nkp.Children(); err != nil {
		return "", err
	}

	for _, child := range children {
		var cp, ch string
		if cp, err = child.Path(); err != nil {
			return "", err
		}

		childPaths = append(childPaths, cp)
		if ch, err = child.Hash(hashmap); err != nil {
			return "", err
		}
		h.Write([]byte(ch))
	}

	if timestamp, err = nkp.Timestamp(); err != nil {
		return "", err
	}

	result := hex.EncodeToString(h.Sum(nil))

	n := CreateRegNodeKey(name, path, result, className, childPaths, timestamp)
	hashmap[path] = n
	return result, nil
}

func createNkp(reader io.ReadSeeker, offset uint32, basePath string) *Nkp {
	nkp := &Nkp{basePath: basePath}
	nkp.reader = reader
	nkp.offset = offset

	if sig, err := nkp.sig(); err == nil {
		if sig != 0x6B6E {
			panic(fmt.Sprintf("Bad Signature at offset: 0x%04x\n", offset))
		}
	}

	return nkp
}

// NodeType returns the node type of this key
func (nkp *Nkp) NodeType() (uint16, error) {
	return nkp.readUint16(0x6)
}

// Timestamp returns the Last Modified? time for this key
func (nkp *Nkp) Timestamp() (result time.Time, err error) {
	var timestamp uint64
	if timestamp, err = nkp.readUint64(0x8); err != nil {
		return time.Now(), err
	}

	return parseWindowsTimestamp(int64(timestamp)), nil
}

// OffsetToParent returns the offset (relative to hbin0) of the parent of this key
func (nkp *Nkp) OffsetToParent() (uint32, error) {
	return nkp.readUint32(0x14)
}

func (nkp *Nkp) numSubKeys() (uint32, error) {
	return nkp.readUint32(0x18)
}

func (nkp *Nkp) offsetToSubkeyList() (uint32, error) {
	return nkp.readUint32(0x20)
}

func (nkp *Nkp) numValues() (uint32, error) {
	return nkp.readUint32(0x28)
}

func (nkp *Nkp) offsetToValueList() (uint32, error) {
	return nkp.readUint32(0x2C)
}

func (nkp *Nkp) offsetToSk() (uint32, error) {
	return nkp.readUint32(0x30)
}

func (nkp *Nkp) classNameOffset() (uint32, error) {
	return nkp.readUint32(0x34)
}

func (nkp *Nkp) nameLength() (uint16, error) {
	return nkp.readUint16(0x4C)
}

func (nkp *Nkp) classNameLength() (uint16, error) {
	return nkp.readUint16(0x4e)
}

// ClassName returns the class name of this Registry Key
func (nkp *Nkp) ClassName() (result string, err error) {
	var classNameLength uint16
	var classNameOffset uint32
	var barray []byte
	if classNameLength, err = nkp.classNameLength(); err != nil {
		return "", err
	}

	if classNameOffset, err = nkp.classNameOffset(); err != nil {
		return "", err
	}

	if classNameOffset == 0xffffffff || classNameOffset == 0x0 {
		return "", nil
	}

	if barray, err = nkp.readBytesFromStart(int64(classNameOffset+4), int(classNameLength)); err != nil {
		return "", err
	}

	return UnpackUtf16(barray), nil
	//return hex.EncodeToString(barray), nil
}

// Name returns the name of this Registry Key
func (nkp *Nkp) Name() (result string, err error) {
	var nameLength uint16
	var barray []byte
	if nameLength, err = nkp.nameLength(); err != nil {
		return "", err
	}
	if barray, err = nkp.readBytes(0x50, int(nameLength)); err != nil {
		return "", err
	}
	return string(barray), nil
}

// Path returns the full path of this Registry Key
func (nkp *Nkp) Path() (result string, err error) {
	var name string
	if name, err = nkp.Name(); err != nil {
		return "", err
	}
	return nkp.basePath + "/" + name, nil
}
