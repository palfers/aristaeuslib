package aristaeus

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
)

func createIterator(src io.ReadSeeker) (*HashIterator, error) {
	var buffer []byte
	var nkp *Nkp

	rfb := createRegFblock(src)

	if size, err := rfb.HbinsSize(); err == nil {
		buffer = make([]byte, size)
	} else {
		return nil, err
	}

	if seek, err := src.Seek(4096, 0); err != nil || seek != 4096 {
		return nil, errors.New("failed to seek to first hbin offset")
	}

	br, err := src.Read(buffer)

	if err != nil {
		return nil, err
	}

	if br != len(buffer) {
		return nil, err
	}
	rseeker := bytes.NewReader(buffer)

	if offset, err := rfb.FirstKeyOffset(); err == nil {
		nkp = createNkp(rseeker, offset, "")
	}

	hashmap := make(map[string]RegNodePtr)

	if _, err := nkp.Hash(hashmap); err != nil {
		return nil, err
	}

	return CreateIterator(hashmap), nil
}

func createIteratorFromFile(src string) (*HashIterator, error) {
	var f *os.File
	var err error
	if !fileExists(src) {
		return nil, fmt.Errorf("The specified file (%s) does not exist", src)
	}

	if f, err = os.Open(src); err != nil {
		return nil, err
	}

	defer f.Close()

	//i, e := createIterator(f)
	//return i, e
	return createIterator(f)
}
func createIteratorFromBytes(src []byte) (*HashIterator, error) {
	f := bytes.NewReader(src)
	return createIterator(f)
}

// CompareHiveFiles compares two hives
func CompareHiveFiles(srcA, srcB string) (*HiveDiff, error) {
	ittA, err := createIteratorFromFile(srcA)
	if err != nil {
		return nil, fmt.Errorf("failed to read file: %s", err)
	}

	ittB, err := createIteratorFromFile(srcB)
	if err != nil {
		return nil, fmt.Errorf("failed to read file: %s", err)
	}

	hd := CreateHiveDiff("Changes", fmt.Sprintf("Changes in hive between %s and %s", srcA, srcB), Diff(ittA, ittB))
	return hd, nil
}

/*
// CompareHiveBytes compares two hives
func CompareHiveBytes(srcA, srcB []byte) (*HiveDiff, error) {
	//func CompareHiveBytes(srcA, srcB []byte) ([]byte, string, error) {
	ittA, err := createIteratorFromBytes(srcA)
	if err != nil {
		//return nil, "", fmt.Errorf("failed to read file: %s", err)
		return nil, fmt.Errorf("failed to read file: %s", err)
	}

	ittB, err := createIteratorFromBytes(srcB)
	if err != nil {
		//return nil, "", fmt.Errorf("failed to read file: %s", err)
		return nil, fmt.Errorf("failed to read file: %s", err)
	}

	hd := CreateHiveDiff("Changes", fmt.Sprintf("Changes in hive between %s and %s", srcA, srcB), Diff(ittA, ittB))
	//data, err := json.MarshalIndent(hd, "", "  ")
	//return data, hd.ID, err
	return hd, nil
}
*/

// Parse parses the specified input hive
func (hive *Hive) parse(itt *HashIterator) error {
	n := itt.Current()
	for n != nil {
		if rn, ok := n.(RegNodePtr); ok {
			hive.Map[rn.Path()] = rn
		}

		n = itt.Step()
	}
	return nil
}

// ParseFromBytes parses the specified input hive
func (hive *Hive) ParseFromBytes(src []byte) error {
	var itt *HashIterator
	var err error

	if itt, err = createIteratorFromBytes(src); err != nil {
		return err
	}
	return hive.parse(itt)
}

// ParseFromFile parses the specified input hive
func (hive *Hive) ParseFromFile(src string) error {
	var itt *HashIterator
	var err error

	if itt, err = createIteratorFromFile(src); err != nil {
		return err
	}
	return hive.parse(itt)
}
