package aristaeus

import (
	"fmt"
	"io"
	"time"
)

// RegFblock is a lazy reader to read from the RegFBlock data structure
type RegFblock struct {
	memoryReader
}

func createRegFblock(reader io.ReadSeeker) *RegFblock {
	rp := &RegFblock{}
	rp.reader = reader
	rp.offset = 0

	return rp
}

// ID returns the id for the RegFBlock (should be )
func (rb *RegFblock) ID() (uint32, error) {
	return rb.readUint32(0)
}

// HiveSequence1 returns the first hive sequence
func (rb *RegFblock) HiveSequence1() (uint32, error) {
	return rb.readUint32(4)
}

// HiveSequence2 returns the second hive sequence
func (rb *RegFblock) HiveSequence2() (uint32, error) {
	return rb.readUint32(8)
}

// ModificationTimestamp returns the last modified timestamp for the hive
func (rb *RegFblock) ModificationTimestamp() (time.Time, error) {
	var ts uint64
	var err error

	if ts, err = rb.readUint64(12); err != nil {
		return time.Now(), err
	}
	return parseWindowsTimestamp(int64(ts)), nil
}

// MajorVersion returns the major version of the hive
func (rb *RegFblock) MajorVersion() (uint32, error) {
	return rb.readUint32(20)
}

// MinorVersion returns the minor version of the hive
func (rb *RegFblock) MinorVersion() (uint32, error) {
	return rb.readUint32(24)
}

// FileType returns the file type of the hive
func (rb *RegFblock) FileType() (uint32, error) {
	return rb.readUint32(28)
}

// FileFormat returns the file format of the hive
func (rb *RegFblock) FileFormat() (uint32, error) {
	return rb.readUint32(32)
}

// FirstKeyOffset returns the offset to the first node (relative to the first hBin)
func (rb *RegFblock) FirstKeyOffset() (uint32, error) {
	return rb.readUint32(36)
}

// HbinsSize returns the size of the hive
func (rb *RegFblock) HbinsSize() (uint32, error) {
	return rb.readUint32(40)
}

// ClusteringFactor returns the clustering factor of the hive
func (rb *RegFblock) ClusteringFactor() (uint32, error) {
	return rb.readUint32(44)
}

// HiveName returns the name of the hive
func (rb *RegFblock) HiveName() (string, error) {
	var raw []byte
	var err error

	if raw, err = rb.readBytes(48, 64); err != nil {
		return "", err
	}
	return UnpackUtf16(raw), nil
}

// HiveFlags returns the flags of the hive
func (rb *RegFblock) HiveFlags() (uint32, error) {
	return rb.readUint32(144)
}

// CheckSum returns the checksum stored in the header
func (rb *RegFblock) CheckSum() (uint32, error) {
	return rb.readUint32(508)
}

// Validate validates the RegFBlock header
func (rb *RegFblock) Validate() error {
	var err error
	var value uint32

	if value, err = rb.ID(); err != nil {
		return err
	}

	if value != 0x66676572 { // 'regf'
		return fmt.Errorf("invalid signature, expecting 0x66676572 got 0x%08x", value)
	}

	if value, err = rb.calculateChecksum(); err != nil {
		return err
	}
	cs := value

	if value, err = rb.CheckSum(); err != nil {
		return err
	}

	if value == cs {
		return fmt.Errorf("incorrect checksum. stored value: 0x%08x, calulcated value: 0x%08x", value, cs)
	}
	return nil
}

func (rb *RegFblock) calculateChecksum() (uint32, error) {
	var xsum, idx uint32
	var i uint32
	var err error

	for idx <= 0x1FB {
		if i, err = rb.readUint32(idx); err != nil {
			return 0, err
		}

		xsum ^= i
		idx += 4
	}

	if xsum == 0 {
		return 1, nil
	}
	if xsum == 0xFFFFFFFF {
		return 0xFFFFFFFE, nil
	}
	return xsum, nil
}
