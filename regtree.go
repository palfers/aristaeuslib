package aristaeus

import (
	"time"
)

// NodeType The Type of the Node
type NodeType int

const (
	// Key A registry Key
	Key NodeType = iota
	// Value A registry Value
	Value
)

// RegNode represents a single registry node in the tree
type RegNode struct {
	Apath      string     `json:"path,omitempty"`
	Aname      string     `json:"name,omitempty"`
	AclassName string     `json:"classname,omitempty"`
	Ahash      string     `json:"hash,omitempty"`
	Adatahash  string     `json:"data_hash,omitempty"`
	Achildren  []string   `json:"children,omitempty"`
	Atimestamp *time.Time `json:"timestamp,omitempty"`
	AdataType  uint16     `json:"datatype,omitempty"`
	AdataSize  uint16     `json:"datasize,omitempty"`
	Atype      string     `json:"type,omitempty"`
	Adata      []byte     `json:"data,omitempty"`
}

// RegNodePtr is a pointer to a Reg Node
type RegNodePtr struct {
	Value *RegNode
}

// CreateRegNodeKey create a new reg node
func CreateRegNodeKey(name, path, hash, className string, children []string, timestamp time.Time) RegNodePtr {
	ts := time.Time(timestamp)
	//return RegNode{Apath: path, Ahash: hash, Achildren: children, Atimestamp: &ts, AnodeType: Key}
	val := &RegNode{Aname: name, Apath: path, Ahash: hash, AclassName: className, Achildren: children, Atimestamp: &ts, Atype: "key"}
	return RegNodePtr{Value: val}
}

// CreateRegNodeValue create a new reg node
func CreateRegNodeValue(name, path, hash string, dataType, dataSize uint16, dataHash string, data []byte) RegNodePtr {
	//return RegNode{Apath: path, Ahash: hash, Achildren: []string{}, Atimestamp: nil, AnodeType: Value}

	val := &RegNode{
		Aname:      name,
		Apath:      path,
		Ahash:      hash,
		Achildren:  []string{},
		Atimestamp: nil,
		Atype:      "value",
		AdataType:  dataType,
		AdataSize:  dataSize,
		Adatahash:  dataHash,
		Adata:      data,
	}
	return RegNodePtr{Value: val}
}

// IsEmpty returns false if this node has children, true if there are no children (i.e. leaf node)
func (node RegNodePtr) IsEmpty() bool {
	return len(node.Value.Achildren) == 0
}

// Name returns the name of this node
func (node RegNodePtr) Name() string {
	return node.Value.Aname
}

// Path returns the path to this node
func (node RegNodePtr) Path() string {
	return node.Value.Apath
}

// Timestamp returns the last modified timestamp of this node
func (node RegNodePtr) Timestamp() *time.Time {
	return node.Value.Atimestamp
}

// NodeType returns the type of node
/*
func (node RegNode) NodeType() NodeType {
	return node.AnodeType
}
*/
// NodeType returns the type of node
func (node RegNodePtr) NodeType() string {
	return node.Value.Atype
}

// Hash returns the hash of this node
func (node RegNodePtr) Hash() string {
	return node.Value.Ahash
}

// Children returns paths to children of this node
func (node RegNodePtr) Children() []string {
	return node.Value.Achildren
}

// Diff Calculates the differences between two trees
func Diff(iterA, iterB *HashIterator) []Change {
	var changes []Change

	for iterA.MoreResults() || iterB.MoreResults() {
		c := iterA.Compare(iterB)

		if c > 0 {

			changes = append(changes, Change{Action: Insert, From: nil, To: iterB.Current()})
			iterB.Next()
		} else if c < 0 {
			changes = append(changes, Change{Action: Delete, From: iterA.Current(), To: nil})
			iterA.Next()
		} else {
			if iterA.CompareHash(iterB) {
				// no change
				iterB.Next()
				iterA.Next()
			} else {
				nA := iterA.Current()
				nB := iterB.Current()

				if nA.NodeType() == "value" && nB.NodeType() == "value" {
					changes = append(changes, Change{Action: Modify, From: nA, To: nB})
					iterB.Next()
					iterA.Next()
				} else if nA.NodeType() == "value" || nB.NodeType() == "value" {
					// recursive delete a, recursive insert b
					changes = append(changes, Change{Action: Modify, From: nA, To: nB})
					iterB.Next()
					iterA.Next()
				} else {
					if nA.IsEmpty() {
						changes = append(changes, Change{Action: Insert, From: nil, To: nB})
						iterA.Next()
						iterB.Next()
					} else if nB.IsEmpty() {
						changes = append(changes, Change{Action: Delete, From: nA, To: nil})
						iterA.Next()
						iterB.Next()
					} else {
						changes = append(changes, Change{Action: Modify, From: nA, To: nB})
						iterA.Step()
						iterB.Step()
					}
				}
			}
		}
	}

	return changes
}
