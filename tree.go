package aristaeus

import (
	"errors"
	"fmt"
	"sort"
	"strings"
)

// Node interface represents a single node in the tree
type Node interface {
	IsEmpty() bool
	Path() string
	Name() string
	Hash() string
	Children() []string
	NodeType() string
}

// HashIterator structure used to traverse the hive tree
type HashIterator struct {
	hashmap map[string]RegNodePtr
	sorted  []string
	cursor  int
}

// CreateIterator creates a tree iterator from a map of path/node values
func CreateIterator(input map[string]RegNodePtr) *HashIterator {
	hi := &HashIterator{cursor: 0, hashmap: input}

	for k := range input {
		hi.sorted = append(hi.sorted, k)
	}

	sort.Strings(hi.sorted)
	return hi
}

// MoreResults returns true if there are more nodes in this tree, false otherwise
func (hi *HashIterator) MoreResults() bool {
	return hi.cursor < len(hi.sorted)-1
}

// Current Gets the current node of the tree iterator
func (hi *HashIterator) Current() Node {
	return hi.hashmap[hi.sorted[hi.cursor]]
}

// NodeAt Gets the node at the specified path
func (hi *HashIterator) NodeAt(path string) (Node, error) {
	var val RegNodePtr
	var ok bool
	if val, ok = hi.hashmap[path]; ok {
		return val, nil
	}
	return nil, errors.New("the specified path does not exst")
}

// Compare compares the path (lexographically) of the current node in each tree
func (hi *HashIterator) Compare(iterB *HashIterator) int {
	nA := hi.Current()
	nB := iterB.Current()

	return strings.Compare(nA.Path(), nB.Path())
}

// CompareHash compares the hash values of the current node in each tree
func (hi *HashIterator) CompareHash(iterB *HashIterator) bool {
	nA := hi.Current()
	nB := iterB.Current()

	return nA.Hash() == nB.Hash()
}

// Step returns the first decendant of the current node, or the next sibling node if there are no decendants
func (hi *HashIterator) Step() Node {
	if len(hi.sorted)-1 == hi.cursor {
		// end of map
		return nil
	}
	hi.cursor++

	return hi.Current()
}

// Next returns the next Key/Value of the tree, i.e. the next node at the same depth or higher as the current node
func (hi *HashIterator) Next() Node {
	depth := strings.Count(hi.sorted[hi.cursor], "/")
	for hi.cursor < len(hi.sorted)-1 {
		hi.cursor++
		newDepth := strings.Count(hi.sorted[hi.cursor], "/")
		if newDepth <= depth {
			return hi.Current()
		}
	}

	return nil
}

// Action is the type of change between two trees
type Action string

const (
	// Insert A Key or Value has been inserted into the second tree
	Insert Action = "INSERT"
	// Delete A Key or Value has been deleted from the first tree
	Delete = "DELETE"
	// Modify A Key or Value has been modified
	Modify = "MODIFY"
)

// Change represents a singal difference between two tree structures
type Change struct {
	Action Action
	From   Node // To the modified node in A or nil if Action is Insert
	To     Node // To the modified node in B or nil if Action is Delete
}

// ToString returns the Change structure as a nice string
func (change *Change) ToString() string {
	//var n Node
	//var a string
	switch change.Action {

	case Insert:
		return fmt.Sprintf("Inserted: %s", change.To.Path())
		//n = *change.To
		//a = "Inserted"
	case Modify:
		//n = *change.From
		//a = "Modified"
		return fmt.Sprintf("Modified: %s", change.From.Path())
	case Delete:
		//n = *change.From
		//a = "Deleted"
		return fmt.Sprintf("Deleted: %s", change.From.Path())
	}
	//return fmt.Sprintf("%s: %s", a, n.Path())
	return ""
}
