package aristaeus

import (
	"encoding/binary"
	"fmt"
	"os"
	"time"
	"unicode/utf16"
)

func parseWindowsTimestamp(qword int64) time.Time {
	// A file time is a 64-bit value that represents the number of 100-nanosecond
	// intervals that have elapsed since 12:00 A.M. January 1, 1601 (UTC)
	return time.Unix(0, 100*(qword-116444736000000000))
}

// UnpackUtf16 unpacks a byte array as utf-16 encoded data and returns a string
func UnpackUtf16(data []byte) string {
	var wString []uint16
	wString = make([]uint16, len(data)/2)
	for i := 0; i < len(wString); i++ {
		s := i * 2
		wString[i] = binary.LittleEndian.Uint16(data[s : s+2])
	}
	l := len(wString)
	if l > 0 && wString[l-1] == 0 {
		return string(utf16.Decode(wString[:l-1])) // trim null char if present
	}
	return string(utf16.Decode(wString))
}

// UnpackUtf16Array unpacks a byte array as utf-16 encoded data and returns an array of strings
func UnpackUtf16Array(data []byte) []string {
	var result []string
	var current uint16
	start := 0

	result = make([]string, 1)

	for end := 0; end < len(data)-2; {
		end += 2
		current = binary.LittleEndian.Uint16(data[end : end+2])

		if current == 0 && start != end {
			result = append(result, UnpackUtf16(data[start:end]))
			start = end + 2
		}
	}

	return result
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func fileExists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func hd(b []byte) {
	for i := 0; i < len(b); i += 16 {
		nice := ""
		end := min(i+16, len(b))
		for _, v := range b[i:end] {
			fmt.Printf("0x%02x ", v)
			if v > 20 && v < 128 {
				nice += fmt.Sprintf("%c", v)
			} else {
				nice += "."
			}
		}
		fmt.Println("| ", nice)
	}
}
