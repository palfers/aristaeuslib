package aristaeus

import (
	"crypto/sha512"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
)

// Vkp lightweight point to a registry value, which lazy reads from the underlying data when queried
type Vkp struct {
	memoryReader
	basePath string
}

type valueListPtr struct {
	numEntries uint32
	memoryReader
}

func createVkp(reader io.ReadSeeker, offset uint32, basePath string) *Vkp {
	vkp := &Vkp{}
	vkp.reader = reader
	vkp.offset = offset
	vkp.basePath = basePath

	return vkp
}

// Name returns the name of this Value
func (vkp *Vkp) Name() (result string, err error) {
	var nameLength, namePresentFlag uint16
	var barray []byte

	if namePresentFlag, err = vkp.namePresentFlag(); err != nil {
		return "", err
	}

	if namePresentFlag&0x1 != 0x1 {
		return "default", nil
	}

	if nameLength, err = vkp.nameLength(); err != nil {
		return "", err
	}

	if barray, err = vkp.readBytes(0x18, int(nameLength)); err != nil {
		return "", err
	}
	return string(barray), nil
}

// Data returns the data stored by of this Value
func (vkp *Vkp) Data() (result []byte, err error) {
	var dataLength uint16
	var offsetToData uint32
	var barray []byte

	if dataLength, err = vkp.dataLength(); err != nil {
		return nil, err
	}

	if dataLength&0x8000 == 0x1 || dataLength <= 4 {
		// inline
		dataLength &= 0x7FFF

		if dataLength > 4 {
			return nil, errors.New("invalid data length")
		}

		if barray, err = vkp.offsetAsData(); err != nil {
			return nil, err
		}

		return barray[:dataLength], nil
	}
	if offsetToData, err = vkp.offsetToData(); err != nil {
		return nil, err
	}

	if barray, err = vkp.readBytesFromStart(int64(offsetToData)+4, int(dataLength)); err != nil {
		return nil, err
	}

	return barray, nil
}

// Hash Calculate has for this value
//func (vkp *Vkp) Hash() ([]byte, error) {
func (vkp *Vkp) Hash() (string, error) {
	var err error
	var name, dataHash string

	h := sha512.New()

	if name, err = vkp.Path(); err != nil {
		fmt.Printf("Bad name: %s\n", err)
		return "", err
	}

	h.Write([]byte(name))

	if dataHash, err = vkp.DataHash(); err != nil {
		fmt.Printf("Bad data hash: %s\n", err)
		return "", err
	}

	h.Write([]byte(dataHash))

	return hex.EncodeToString(h.Sum(nil)), nil
}

// DataHash Calculate hash for this value
func (vkp *Vkp) DataHash() (string, error) {
	var err error
	var data []byte

	h := sha512.New()

	if data, err = vkp.Data(); err != nil {
		fmt.Printf("Bad data: %s\n", err)
		return "", err
	}

	h.Write(data)

	return hex.EncodeToString(h.Sum(nil)), nil
}

func (vkp *Vkp) nameLength() (uint16, error) {
	return vkp.readUint16(0x6)
}

// DataType returns the data type of this value
func (vkp *Vkp) DataType() (uint16, error) {
	return vkp.readUint16(0x10)
}

func (vkp *Vkp) namePresentFlag() (uint16, error) {
	return vkp.readUint16(0x14)
}

func (vkp *Vkp) dataLength() (uint16, error) {
	return vkp.readUint16(0x8)
}

//func (vkp *Vkp) dataLength() (uint32, error) {
//	return vkp.readUint32(0x8)
//}

func (vkp *Vkp) offsetToData() (uint32, error) {
	return vkp.readUint32(0x0c)
}

func (vkp *Vkp) offsetAsData() ([]byte, error) {
	return vkp.readBytes(0x0c, 4)
}

func (vlp *valueListPtr) valueOffsets() ([]uint32, error) {
	if vlp.numEntries == 0 {
		return []uint32{}, nil
	}

	result := make([]uint32, vlp.numEntries)

	vlp.reader.Seek(int64(vlp.offset+4), 0)
	if err := binary.Read(vlp.reader, binary.LittleEndian, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// Path returns the full path of this Registry Key
func (vkp *Vkp) Path() (result string, err error) {
	var name string
	if name, err = vkp.Name(); err != nil {
		return "", err
	}
	return vkp.basePath + "/" + name, nil
}
